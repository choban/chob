# Coban
It's a basic package manager inspired from Arch Linux pacman, built with Python for Windows platform. You can easily install, remove, upgrade packages with only few commands.

## What Packages Are Available?
1. Type `main.py --packages` for a list.
2. Type `main.py -Ss <packageName>` for searching a specific package.
## More Documentation
It will be available soon with the first release.

## Troubleshooting
First, please run `main.py --update` and `main.py --doctor`.

**If you don't read these it will take us far longer to help you with your problem.**

## Security
Please report security issues to our [HackerOne](https://hackerone.com/coban).

## Who Are You?
Coban's lead maintainer is [Muhammed Kaplan](https://github.com/muhammedkpln).

## License
Code is under the [Apache 2 License](LICENSE.txt).
